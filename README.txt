run the following command in a terminal window from project root directory:
mvn clean install
mvn spring-boot:run

You can access the H2 console at http://localhost:8080/console/
Use the following JSBC URL: jdbc:h2:mem:sync_db;
UserName: sa
Password:

