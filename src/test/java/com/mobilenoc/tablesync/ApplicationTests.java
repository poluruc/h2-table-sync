package com.mobilenoc.tablesync;

import com.mobilenoc.tablesync.model.TaskDefinition;
import com.mobilenoc.tablesync.model.TaskDefinitionMirror;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTests {
    private final static Long WAIT_TIME_MILLIS = 500L;

    @Autowired
    private EntityManagerFactory emf;
    @Autowired
    private static EntityManager em;

    @AfterClass
    public static void tearDown() {
        em.close();
    }

    @Test
    public void testEndToEnd() throws InterruptedException {
        startTransaction();
        // insert records into TaskDefinition
        insertDefinitionRecords();
        commitTransaction();

        // Wait for sync
        Thread.sleep(WAIT_TIME_MILLIS);

        verifyMirrors();
        TaskDefinitionMirror taskDefinitionMirror;

        // Verify updates
        startTransaction();
        em.createNativeQuery("UPDATE tbl_task_definition_mirror SET name ='updated_name', description='updated_description'").executeUpdate();
        commitTransaction();

        // Wait for sync
        Thread.sleep(WAIT_TIME_MILLIS);

        updateDefinitionRecords();

        // Verify changes on the Mirror side
        startTransaction();
        changeMirrorSideRecords();
        commitTransaction();

        // Wait for sync
        Thread.sleep(WAIT_TIME_MILLIS);

        // Verify
        TaskDefinition taskDefinition = em.find(TaskDefinition.class, 5);
        assertNotNull(taskDefinition);
        assertEquals("ex name 5", taskDefinition.getName());
        assertEquals("ex description 5", taskDefinition.getDescription());

        taskDefinition = em.find(TaskDefinition.class, 1);
        assertNull(taskDefinition);

        taskDefinitionMirror = em.find(TaskDefinitionMirror.class, 2);
        assertNotNull(taskDefinitionMirror);
        assertEquals("UPDATED NAME", taskDefinitionMirror.getName());

        // verify DELETES
        taskDefinition = em.find(TaskDefinition.class, 1);
        assertNull(taskDefinition);
    }

    private void changeMirrorSideRecords() {
        em.createNativeQuery("INSERT INTO tbl_task_definition_mirror(id, name, description) VALUES(5, 'ex name 5', 'ex description 5')").executeUpdate();
        em.createNativeQuery("DELETE FROM tbl_task_definition_mirror WHERE id = 1").executeUpdate();
        em.createNativeQuery("UPDATE tbl_task_definition SET name = 'UPDATED NAME' WHERE id = 2").executeUpdate();
    }

    private void updateDefinitionRecords() {
        for (int i = 1; i < 5; i++) {
            TaskDefinition taskDefinition = em.find(TaskDefinition.class, i);
            assertNotNull(taskDefinition);
            assertEquals("updated_name", taskDefinition.getName());
            assertEquals("updated_description", taskDefinition.getDescription());
        }
    }

    private void verifyMirrors() {
        TaskDefinitionMirror taskDefinitionMirror = em.find(TaskDefinitionMirror.class, 1);
        assertNotNull(taskDefinitionMirror);
        assertEquals("ex name 1", taskDefinitionMirror.getName());
        assertEquals("ex description 1", taskDefinitionMirror.getDescription());

        taskDefinitionMirror = em.find(TaskDefinitionMirror.class, 2);
        assertNotNull(taskDefinitionMirror);
        assertEquals("ex name 2", taskDefinitionMirror.getName());
        assertEquals("ex description 2", taskDefinitionMirror.getDescription());

        taskDefinitionMirror = em.find(TaskDefinitionMirror.class, 3);
        assertNotNull(taskDefinitionMirror);
        assertEquals("ex name 3", taskDefinitionMirror.getName());
        assertEquals("ex description 3", taskDefinitionMirror.getDescription());

        taskDefinitionMirror = em.find(TaskDefinitionMirror.class, 4);
        assertNotNull(taskDefinitionMirror);
        assertEquals("ex name 4", taskDefinitionMirror.getName());
        assertEquals("ex description 4", taskDefinitionMirror.getDescription());
    }

    private void insertDefinitionRecords() {
        em.createNativeQuery("INSERT INTO tbl_task_definition(id, name, description) VALUES(1, 'ex name 1', 'ex description 1')").executeUpdate();
        em.createNativeQuery("INSERT INTO tbl_task_definition(id, name, description) VALUES(2, 'ex name 2', 'ex description 2')").executeUpdate();
        em.createNativeQuery("INSERT INTO tbl_task_definition(id, name, description) VALUES(3, 'ex name 3', 'ex description 3')").executeUpdate();
        em.createNativeQuery("INSERT INTO tbl_task_definition(id, name, description) VALUES(4, 'ex name 4', 'ex description 4')").executeUpdate();
    }

    private void startTransaction() {
        em = emf.createEntityManager();
        em.getTransaction().begin();
    }

    private void commitTransaction() {
        em.flush();
        em.getTransaction().commit();
        //em.close();
    }

}
