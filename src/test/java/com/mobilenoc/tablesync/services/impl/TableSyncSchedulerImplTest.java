package com.mobilenoc.tablesync.services.impl;

import com.mobilenoc.tablesync.model.TaskDefinition;
import com.mobilenoc.tablesync.model.TaskDefinitionMirror;
import com.mobilenoc.tablesync.repository.TaskDefinitionMirrorRepository;
import com.mobilenoc.tablesync.repository.TaskDefinitionRepository;
import com.mobilenoc.tablesync.services.TableSyncScheduler;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;

import static org.junit.Assert.*;

@DataJpaTest
@RunWith(SpringRunner.class)
public class TableSyncSchedulerImplTest {

    @Autowired
    private EntityManager em;

    @Autowired
    private TaskDefinitionRepository definitionRepo;

    @Autowired
    private TaskDefinitionMirrorRepository mirrorRepo;

    private TableSyncScheduler scheduler;


    @Before
    public void init() {
        scheduler = new TableSyncSchedulerImpl(new TaskDefinitionServiceImpl(definitionRepo, mirrorRepo));
    }


    @Test
    public void startSyncing() {
        // Given
        em.createNativeQuery("INSERT INTO tbl_task_definition(id, name, description) VALUES(1, 'ex name 1', 'ex description 3')").executeUpdate();
        em.createNativeQuery("INSERT INTO tbl_task_definition(id, name, description) VALUES(2, 'ex name 2', null)").executeUpdate();
        em.createNativeQuery("INSERT INTO tbl_task_definition(id, name, description, sync_id) VALUES(3, 'ex name 3', 'ex description 3', 'test_id')").executeUpdate();
        em.createNativeQuery("INSERT INTO tbl_task_definition(id, name, description, sync_id) VALUES(4, 'ex name 4', 'ex description 4', 'updated')").executeUpdate();
        em.createNativeQuery("INSERT INTO tbl_task_definition_mirror(id, name, description, sync_id) VALUES(4, 'name_updated', 'description_updated', 'updated')").executeUpdate();

        // When
        scheduler.startSyncing();

        // Then
        TaskDefinition taskDefinition;
        verifyInserts();
        verifyDeletes();
        verifyUpdates();
    }

    private void verifyUpdates() {
        TaskDefinition taskDefinition;
        taskDefinition = em.find(TaskDefinition.class, 4);
        assertNotNull(taskDefinition);
        assertEquals("name_updated", taskDefinition.getName());
        assertEquals("description_updated", taskDefinition.getDescription());
    }

    private void verifyDeletes() {
        TaskDefinition taskDefinition = em.find(TaskDefinition.class, 3);
        assertNull(taskDefinition);
    }

    private void verifyInserts() {
        TaskDefinitionMirror taskDefinitionMirror = em.find(TaskDefinitionMirror.class, 1);
        assertNotNull(taskDefinitionMirror);
        assertEquals("ex name 1", taskDefinitionMirror.getName());
        assertEquals("ex description 3", taskDefinitionMirror.getDescription());

        taskDefinitionMirror = em.find(TaskDefinitionMirror.class, 2);
        assertNotNull(taskDefinitionMirror);
        assertEquals("ex name 2", taskDefinitionMirror.getName());
        assertNull(taskDefinitionMirror.getDescription());
    }

}
