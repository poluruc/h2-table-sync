package com.mobilenoc.tablesync.model;

import com.mobilenoc.tablesync.model.base.BaseTaskDefinition;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "tbl_task_definition_mirror")
@Data
public class TaskDefinitionMirror extends BaseTaskDefinition {

    @OneToOne
    @JoinColumn(name = "id")
    private TaskDefinition definition;


    public TaskDefinitionMirror() {
    }

    public TaskDefinitionMirror(Integer id, @NotNull String name, String description, String syncId) {
        super(id, name, description, syncId);
    }


    public static TaskDefinitionMirror from(TaskDefinition t) {
        if (Objects.isNull(t)) {
            return null;
        }
        return new TaskDefinitionMirror(t.getId(), t.getName(), t.getDescription(), t.getSyncId());
    }

}
