package com.mobilenoc.tablesync.model;

import com.mobilenoc.tablesync.model.base.BaseTaskDefinition;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "tbl_task_definition")
@Data
public class TaskDefinition extends BaseTaskDefinition {

    @OneToOne
    @JoinColumn(name = "id")
    private TaskDefinitionMirror mirror;


    public TaskDefinition() {
    }

    public TaskDefinition(Integer id, @NotNull String name, String description, String syncId) {
        super(id, name, description, syncId);
    }

    public static TaskDefinition from(TaskDefinitionMirror m) {
        if (Objects.isNull(m)) {
            return null;
        }

        return new TaskDefinition(m.getId(), m.getName(), m.getDescription(), m.getSyncId());
    }

}
