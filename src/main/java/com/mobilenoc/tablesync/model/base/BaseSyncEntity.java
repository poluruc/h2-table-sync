package com.mobilenoc.tablesync.model.base;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;

@MappedSuperclass
@Data
@EqualsAndHashCode(exclude= {"updatedTS"})
public abstract class BaseSyncEntity extends BaseEntity {

    // Allow only updates
    @Column(name = "update_timestamp", insertable = false, updatable = true)
    protected Timestamp updatedTS;

    @Column(name = "sync_id")
    protected String syncId;

    @PrePersist
    @PreUpdate
    public void updateSyncId() {
        //Set by the system to mark the entity as synced
        if (Objects.isNull(syncId)) {
            syncId = UUID.randomUUID().toString();
        }
    }
}
