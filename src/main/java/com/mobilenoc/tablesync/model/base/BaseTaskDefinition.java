package com.mobilenoc.tablesync.model.base;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

@MappedSuperclass
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"syncId"})
public abstract class BaseTaskDefinition extends BaseSyncEntity {

    @NotNull
    @Column(name = "name")
    protected String name;

    @Column(name = "description")
    protected String description;


    public BaseTaskDefinition(@NotNull Integer id, @NotNull String name, String description, String syncId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.syncId = syncId;
    }
}
