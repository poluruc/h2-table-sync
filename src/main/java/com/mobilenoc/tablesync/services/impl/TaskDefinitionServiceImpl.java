package com.mobilenoc.tablesync.services.impl;

import com.mobilenoc.tablesync.model.TaskDefinition;
import com.mobilenoc.tablesync.model.TaskDefinitionMirror;
import com.mobilenoc.tablesync.model.base.BaseTaskDefinition;
import com.mobilenoc.tablesync.repository.TaskDefinitionMirrorRepository;
import com.mobilenoc.tablesync.repository.TaskDefinitionRepository;
import com.mobilenoc.tablesync.services.TaskDefinitionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class TaskDefinitionServiceImpl implements TaskDefinitionService {

    private final TaskDefinitionRepository definitionRepo;
    private final TaskDefinitionMirrorRepository mirrorRepo;


    public TaskDefinitionServiceImpl(@NotNull TaskDefinitionRepository definitionRepo,
                                     @NotNull TaskDefinitionMirrorRepository mirrorRepo) {
        this.definitionRepo = definitionRepo;
        this.mirrorRepo = mirrorRepo;
    }


    @NotNull
    @Override
    public List<TaskDefinition> findAllNewTaskDefinitions() {
        return definitionRepo.findAllBySyncIdIsNull();
    }

    @NotNull
    @Override
    public List<TaskDefinitionMirror> findAllNewTaskDefinitionMirrors() {
        return mirrorRepo.findAllBySyncIdIsNull();
    }

    @Override
    public @NotNull List<TaskDefinition> findAllTaskDefinitionsWithConflicts() {
        return definitionRepo.findAllUpdated();
    }

    @Override
    public void deleteAllDefinitionsWithNoMirror() {
        definitionRepo.findAllByMirror_IdIsNullAndSyncIdIsNotNull()
                .forEach(definitionRepo::delete);
    }

    @Override
    public void deleteAllMirrorsWithNoDefinitions() {
        mirrorRepo.findAllByDefinition_IdIsNullAndSyncIdIsNotNull()
                .forEach(mirrorRepo::delete);
    }

    @Override
    public void createBothFrom(@NotNull TaskDefinition definition) {
        definition = definitionRepo.saveAndFlush(definition);
        mirrorRepo.saveAndFlush(TaskDefinitionMirror.from(definition));
    }

    @Override
    public void createBothFrom(@NotNull TaskDefinitionMirror mirror) {
        mirror = mirrorRepo.saveAndFlush(mirror);
        definitionRepo.saveAndFlush(TaskDefinition.from(mirror));
    }

    @Override
    public void patch(@NotNull TaskDefinitionMirror mirror, @NotNull String name, String description, String syncUuid) {
        patchBaseTask(mirror, name, description, syncUuid);
        mirrorRepo.saveAndFlush(mirror);
    }

    @Override
    public void patch(@NotNull TaskDefinition taskDefinition, @NotNull String name, String description, String syncUuid) {
        patchBaseTask(taskDefinition, name, description, syncUuid);
        definitionRepo.saveAndFlush(taskDefinition);
    }

    private void patchBaseTask(BaseTaskDefinition baseTaskDefinition, @NotNull String name, String description, String syncUuid) {
        baseTaskDefinition.setName(name);
        baseTaskDefinition.setDescription(description);
        baseTaskDefinition.setSyncId(syncUuid);
    }

}
