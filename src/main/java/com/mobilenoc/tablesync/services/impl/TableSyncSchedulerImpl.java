package com.mobilenoc.tablesync.services.impl;

import com.mobilenoc.tablesync.model.TaskDefinition;
import com.mobilenoc.tablesync.model.TaskDefinitionMirror;
import com.mobilenoc.tablesync.services.TableSyncScheduler;
import com.mobilenoc.tablesync.services.TaskDefinitionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;


@Service
public class TableSyncSchedulerImpl implements TableSyncScheduler {

    private final TaskDefinitionService taskDefinitionService;


    public TableSyncSchedulerImpl(TaskDefinitionService taskDefinitionService) {
        this.taskDefinitionService = taskDefinitionService;
    }

    @Transactional
    @Override
    public void startSyncing() {
        sysncDefinitionInserts();
        syncMirrorInserts();

        syncMirrorDeletes();
        syncDefinitionDeletes();

        syncDefinitionUpdates();
    }

    private void syncMirrorInserts() {
        List<TaskDefinitionMirror> mirrors = taskDefinitionService.findAllNewTaskDefinitionMirrors();
        mirrors.forEach(mirror -> {
            if (null == mirror.getDefinition()) {
                taskDefinitionService.createBothFrom(mirror);
            } else {
                syncChangesByUpdatedTS(mirror.getDefinition(), mirror);
            }
        });
    }

    private void sysncDefinitionInserts() {
        List<TaskDefinition> newTaskDefinitions = taskDefinitionService.findAllNewTaskDefinitions();
        newTaskDefinitions.forEach(taskDefinition -> {
            if (null == taskDefinition.getMirror()) {
                taskDefinitionService.createBothFrom(taskDefinition);
            } else {
                syncChangesByUpdatedTS(taskDefinition, taskDefinition.getMirror());
            }
        });
    }

    private void syncDefinitionUpdates() {
        List<TaskDefinition> taskDefinitions = taskDefinitionService.findAllTaskDefinitionsWithConflicts();
        taskDefinitions.forEach(taskDefinition -> syncChangesByUpdatedTS(taskDefinition, taskDefinition.getMirror()));
    }

    private void syncChangesByUpdatedTS(@NotNull TaskDefinition taskDefinition,
                                        @NotNull TaskDefinitionMirror mirror) {
        if (mirror.getUpdatedTS().before(taskDefinition.getUpdatedTS())) {
            taskDefinitionService.patch(mirror,
                    taskDefinition.getName(), taskDefinition.getDescription(), taskDefinition.getSyncId());
            return;
        }
        taskDefinitionService.patch(taskDefinition, mirror.getName(), mirror.getDescription(), mirror.getSyncId());
    }

    private void syncMirrorDeletes() {
        taskDefinitionService.deleteAllDefinitionsWithNoMirror();
    }

    private void syncDefinitionDeletes() {
        taskDefinitionService.deleteAllMirrorsWithNoDefinitions();
    }

}
