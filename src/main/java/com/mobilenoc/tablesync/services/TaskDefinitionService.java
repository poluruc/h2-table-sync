package com.mobilenoc.tablesync.services;

import com.mobilenoc.tablesync.model.TaskDefinition;
import com.mobilenoc.tablesync.model.TaskDefinitionMirror;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface TaskDefinitionService {
    @NotNull List<TaskDefinition> findAllNewTaskDefinitions();
    @NotNull List<TaskDefinitionMirror> findAllNewTaskDefinitionMirrors();
    @NotNull List<TaskDefinition> findAllTaskDefinitionsWithConflicts();
    void deleteAllDefinitionsWithNoMirror();
    void deleteAllMirrorsWithNoDefinitions();
    void createBothFrom(@NotNull TaskDefinition definition);
    void createBothFrom(@NotNull TaskDefinitionMirror mirror);
    void patch(@NotNull TaskDefinitionMirror mirror, @NotNull String name, String description, String syncUuid);
    void patch(@NotNull TaskDefinition taskDefinition, @NotNull String name, String description, String syncUuid);
}
