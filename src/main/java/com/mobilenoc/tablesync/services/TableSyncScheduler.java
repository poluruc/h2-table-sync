package com.mobilenoc.tablesync.services;

public interface TableSyncScheduler {

    void startSyncing();

}
