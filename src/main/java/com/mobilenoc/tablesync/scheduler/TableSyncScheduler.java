package com.mobilenoc.tablesync.scheduler;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class TableSyncScheduler {

    private final com.mobilenoc.tablesync.services.TableSyncScheduler scheduler;


    public TableSyncScheduler(com.mobilenoc.tablesync.services.TableSyncScheduler scheduler) {
        this.scheduler = scheduler;
    }

    @Scheduled(fixedDelay = 100)
    public void run() {
        scheduler.startSyncing();
    }

}
