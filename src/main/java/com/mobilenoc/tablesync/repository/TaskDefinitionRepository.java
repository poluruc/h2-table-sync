package com.mobilenoc.tablesync.repository;

import com.mobilenoc.tablesync.model.TaskDefinition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface TaskDefinitionRepository extends JpaRepository<TaskDefinition, Integer> {

    @NotNull List<TaskDefinition> findAllBySyncIdIsNull();
    @NotNull List<TaskDefinition> findAllByMirror_IdIsNullAndSyncIdIsNotNull();
    @Query("SELECT t FROM TaskDefinition as t JOIN t.mirror as m " +
            "WHERE m.syncId = t.syncId " +
            "   AND (t.name <> m.name " +
            "       OR ((t.description IS NULL AND m.description IS NOT NULL) " +
            "       OR (m.description IS NULL AND t.description IS NOT NULL) " +
            "       OR t.description <> m.description))")
    List<TaskDefinition> findAllUpdated();
}
