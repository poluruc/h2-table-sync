package com.mobilenoc.tablesync.repository;

import com.mobilenoc.tablesync.model.TaskDefinitionMirror;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface TaskDefinitionMirrorRepository extends JpaRepository<TaskDefinitionMirror, Integer> {

    @NotNull List<TaskDefinitionMirror> findAllBySyncIdIsNull();
    @NotNull List<TaskDefinitionMirror> findAllByDefinition_IdIsNullAndSyncIdIsNotNull();
}
