CREATE TABLE tbl_task_definition(
 id INT NOT NULL PRIMARY KEY,
 name VARCHAR(255) NOT NULL,
 description VARCHAR(500) NULL,
 sync_id VARCHAR(36) NULL,
 update_timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE tbl_task_definition_mirror(
 id INT NOT NULL PRIMARY KEY,
 name VARCHAR(255) NOT NULL,
 description VARCHAR(500) NULL,
 sync_id VARCHAR(36) NULL,
 update_timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE INDEX IF NOT EXISTS idx_task_definition_name_desc ON tbl_task_definition(name, description);
CREATE INDEX IF NOT EXISTS idx_task_definition_sync_id ON tbl_task_definition(sync_id);
CREATE INDEX IF NOT EXISTS idx_task_definition_mirror_name_desc ON tbl_task_definition_mirror(name, description);
CREATE INDEX IF NOT EXISTS idx_task_definition_mirror_syncid ON tbl_task_definition_mirror(sync_id);
